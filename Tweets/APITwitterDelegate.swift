//
//  APITwitterDelegate.swift
//  Tweets
//
//  Created by Patricio GUZMAN on 10/6/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import Foundation
import UIKit

protocol APITwitterDelegate: class {
//    var token: String {get set}
    
    func receiveTweets(tweets: [Tweet])
    func createError(error: NSError) throws
    func presentAlert(alert: UIAlertController)
//    func getToken()
}
