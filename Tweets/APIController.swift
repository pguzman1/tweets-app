//
//  APIController.swift
//  Tweets
//
//  Created by Patricio GUZMAN on 10/6/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import Foundation
import UIKit

enum MyError : Error {
    case RuntimeError(String)
}

class APIController {
    weak var delegate: APITwitterDelegate?
    let token: String
    var myTweets: [Tweet] = []
    
    init(delegate: APITwitterDelegate?, token: String) {
        print("creating apicontrolller")
        self.delegate = delegate
        self.token = token
    }
    
    func request100Tweets(searchFor: String) {
        let trimmedString = searchFor.trimmingCharacters(in: .whitespacesAndNewlines)
        if (trimmedString == "") {
            return 
        }
        print("request100tweets")
        let escapedString = searchFor.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
        let myurl = NSURL(string: "https://api.twitter.com/1.1/search/tweets.json?lang=fr&count=100&q=" + escapedString!)
        let myrequest = NSMutableURLRequest(url: myurl! as URL)
        myrequest.httpMethod = "GET"
        myrequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        let task = URLSession.shared.dataTask(with: myrequest as URLRequest) {
            (data, response, error) in
            if let err = error {
                print(err)
            }
            else if let d = data {
                do {
                    if let dic = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any] {
                        let tweets = dic["statuses"] as! [[String:Any]]
                        self.myTweets = []
                        var dateOfTweet = ""
                        var textOfTweet = ""
                        var nameOfTweet = ""
                        //  THIS IS FOR THROW AN ERROR
//                        let err = MyError.RuntimeError("BOOM")
//                        do {
//                            try self.delegate?.createError(error: err as NSError)
//                        } catch MyError.RuntimeError(let errorMessage) {
//                            let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
//                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
//                                alert.dismiss(animated: true, completion: nil)
//                                
//                            }))
//                            self.delegate?.presentAlert(alert: alert)
//                        }
                        for tweet in tweets {
                            if let created = tweet["created_at"] as? String {
                                dateOfTweet = created
                            }
                            if let text = tweet["text"] as? String {
                                textOfTweet = text
                            }
                            if let user = tweet["user"] as? [String:Any] {
                                if let name = user["name"] as? String {
                                    nameOfTweet = name
                                }
                            }
                            self.myTweets.append(Tweet(name: nameOfTweet, text: textOfTweet, data: dateOfTweet, description: nameOfTweet + " wrote : \"" + textOfTweet + "\" on " + dateOfTweet))
                        }
                        print("printing tweets")
                    }
                    DispatchQueue.main.async(){
                        print("DISPATCHING\n\n")
                        self.delegate?.receiveTweets(tweets: self.myTweets)
                    }
                }
                catch(let error) {
                    print("ERROR IN CATCH")
                    print(error)
                    let alert = UIAlertController(title: "Error", message: "Something went wrong", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        alert.dismiss(animated: true, completion: nil)
                        
                    }))
                    self.delegate?.presentAlert(alert: alert)
                }
            }
            
        }
        task.resume()
    }
    
}
