//
//  Tweet.swift
//  Tweets
//
//  Created by Patricio GUZMAN on 10/6/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import Foundation

struct Tweet: CustomStringConvertible {
    let name: String
    let text: String
    let data: String
    let description: String
}
