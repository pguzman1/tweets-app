//
//  ViewController.swift
//  Tweets
//
//  Created by Patricio GUZMAN on 10/6/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit

class ViewController: UIViewController, APITwitterDelegate {
    
    var apiController: APIController!
    var token: String = ""
    var myTweetsToDisplay: [Tweet] = []
    var cellHeights: [IndexPath : CGFloat] = [:]
    
    @IBOutlet weak var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getToken()
        table.estimatedRowHeight = 300
        table.rowHeight = UITableViewAutomaticDimension
    }
    
    func receiveTweets(tweets: [Tweet]) {
        myTweetsToDisplay = tweets
        print(" TWEEETS => \(myTweetsToDisplay.count))")
        table.reloadData()
    }
    
    func presentAlert(alert: UIAlertController) {
        self.present(alert, animated: true, completion: nil)
    }
    
    func setToken (token: String) {
        self.token = token
    }
    
    func createError(error: NSError) throws {
        throw error
    }
    
    func getToken(){
        request.httpMethod = "POST"
        request.setValue("Basic " + BEARER, forHTTPHeaderField: "Authorization")
        request.setValue("application/x-www-form-urlencoded;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = "grant_type=client_credentials".data(using: String.Encoding.utf8)
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            (data, response, error) in
            if let err = error {
                print(err)
            }
            else if let d = data {
                do {
                    if let dic: NSDictionary = try JSONSerialization.jsonObject(with: d, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                        if (dic["access_token"] as? String) != nil {
                            self.token = dic["access_token"] as! String
                            self.apiController = APIController(delegate: self, token: self.token)
                            self.apiController.request100Tweets(searchFor: "ecole 42")
                        }
                    }
                }
                catch(let error) {
                    let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        alert.dismiss(animated: true, completion: nil)
                        
                    }))
                    self.presentAlert(alert: alert)
                }
            }
            
        }
        task.resume()
    }
    
    func search(forThis: String) {
        self.apiController.request100Tweets(searchFor: forThis)
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let cell = table.dequeueReusableCell(withIdentifier: "searchBarIdentifier") as! TextFieldTableViewCell
            cell.searchBar.delegate = self
            cell.delegate = self
            return cell
        }
        let cell = table.dequeueReusableCell(withIdentifier: "MyTableViewCell") as! MyTableViewCell
        if (indexPath.row >= myTweetsToDisplay.count) {
            return cell
        }
        let tweetForCell = myTweetsToDisplay[indexPath.row]
        let nameForCell = tweetForCell.name
        let dateForCell = tweetForCell.data
        let textForCell = tweetForCell.text
        cell.setCell(nameCell: nameForCell, dateCell: dateForCell, textoCell: textForCell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myTweetsToDisplay.count + 1
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let height = cellHeights[indexPath] else { return 300 }
        return height
    }
    
}

extension ViewController: UITextFieldDelegate, TextFieldTableViewCellDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.search(forThis: textField.text!)
        textField.resignFirstResponder()
        return true
    }
    
    func didTapButton(_ sender: UIButton, textToSearch: String) {
        self.search(forThis: textToSearch)
    }
    
    func touchesBeganDelegate(textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
}

