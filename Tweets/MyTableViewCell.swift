//
//  MyTableViewCell.swift
//  Tweets
//
//  Created by Patricio GUZMAN on 10/6/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit

class MyTableViewCell: UITableViewCell {

    @IBOutlet weak var UiView: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var texto: UILabel!
    
    func setCell(nameCell: String, dateCell: String, textoCell: String) {
        name.text = nameCell
        if dateCell.characters.count > 11 {
            date.text = String(dateCell.characters.dropLast(11))
        }
        else {
            date.text = dateCell
        }
        texto.text = textoCell
        UiView.layer.cornerRadius = 5;
        UiView.layer.masksToBounds = true;
    }
    

}
