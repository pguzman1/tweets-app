//
//  TextFieldTableViewCell.swift
//  Tweets
//
//  Created by Patricio GUZMAN on 10/6/17.
//  Copyright © 2017 Patricio GUZMAN. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    var delegate: TextFieldTableViewCellDelegate?

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        delegate?.touchesBeganDelegate(textField: searchBar)
    }
    
    @IBAction func didSearch(_ sender: Any) {
        delegate?.didTapButton(searchButton, textToSearch: self.searchBar.text!)
    }
}

protocol TextFieldTableViewCellDelegate: class {
    func didTapButton(_ sender: UIButton, textToSearch: String)
    func touchesBeganDelegate(textField: UITextField)
}
